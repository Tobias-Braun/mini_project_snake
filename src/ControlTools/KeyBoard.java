package controlTools;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class KeyBoard implements KeyListener{
	private static boolean[] codes = new boolean[1024];
	
	private static boolean keyValid(int KeyCode){
		if (KeyCode>0 && KeyCode < codes.length) return true;
		else return false;
	}
	
	public static boolean iskeyPressed(int KeyCode){
		if(keyValid(KeyCode)) return codes[KeyCode];
		else return false;
	}
	@Override
	public void keyPressed(KeyEvent e) {
		/*
		 * changes Boolean value of KeyCode to true when Key is pressed
		 */
		int code = e.getKeyCode();
		if (keyValid(code)) codes[code] = true;
		if (code == KeyEvent.VK_ESCAPE)
			System.exit(1);
	}

	@Override
	public void keyReleased(KeyEvent e) {
		/* 
		 * changes Boolean value of KeyCode to false when Key is released
		 */
		int code = e.getKeyCode();
		if (keyValid(code)) codes[code] = false;	
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		//unn�tig		
	}

}
