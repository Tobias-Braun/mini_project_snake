package controlTools;

import javax.swing.JFrame;

import constants.C;

@SuppressWarnings("serial")
public class Frame extends JFrame{
	
	private GamePanel panel = new GamePanel();
	
	public void paint(int l) {
		setTitle("Snake - current length: " + l);
		panel.repaint();
	}
	
	public Frame(){
		super("Snake");
		addKeyListener(new KeyBoard());
		this.setBounds(80, 60, C.PANELWIDTH, C.PANELHEIGHT + 40);
		panel.setBounds(0, 0, C.PANELWIDTH, C.PANELHEIGHT);
		add(panel);
		setResizable(false);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setUndecorated(false);
		setVisible(true);
		requestFocus();
	}

}