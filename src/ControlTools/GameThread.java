package controlTools;

import java.awt.event.KeyEvent;
import java.util.LinkedList;
import java.util.Random;

import constants.C;

public class GameThread extends Thread {
	
	public static LinkedList<Zelle> positionen = new LinkedList<Zelle>();
	public static Zelle food;
	private static boolean[] keys = new boolean[4];
	private Frame window;
	public static int lastKey;
	private Random r = new Random();
	
	
	@Override
	public void run() {
		window = new Frame();
		init();
		long timeSinceLastFrame = 500;
		long timeBuffer;
		while(true){
			
			timeBuffer = System.currentTimeMillis();
			updateKeys();

			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			timeSinceLastFrame += System.currentTimeMillis() - timeBuffer;
			if(timeSinceLastFrame>C.FPS){
				timeSinceLastFrame -= C.FPS;
				update();
				window.paint(positionen.size());
			}
		}
	}
	
	private void init(){
		positionen.add(new Zelle(3, 0));
		positionen.add(new Zelle(2, 0));
		positionen.add(new Zelle(1, 0));
		positionen.add(new Zelle(0, 0));
		lastKey = KeyEvent.VK_D;
		updateFood();
		try {
			Thread.sleep(15);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void update(){
		if (positionen.size() == (C.PANELWIDTH / C.TILESIZE) * (C.PANELHEIGHT / C.TILESIZE) - 1) {
			restart();
		}
		if (positionen.isEmpty()) return;
		Zelle next = getNextCell();
		positionen.addFirst(next);
		if (!(next.getX() == food.getX() && next.getY() == food.getY())) positionen.removeLast();
		else updateFood();
		for (int i = 1;i<positionen.size(); i++){
			if (positionen.get(i).x == next.getX() && positionen.get(i).y == next.getY()) restart();
		}
		if (next.getX() < 0 || next.getY() < 0 || next.getX() > (C.PANELWIDTH/C.TILESIZE)-1 || next.getY() > (C.PANELHEIGHT/C.TILESIZE)-1) restart();
	}
	
	public static void setKey(int code, boolean down){
		keys[code] = down;
	}
	
	
	private void restart(){
		positionen.clear();
		init();
	}
	
	private void updateKeys(){
		if (KeyBoard.iskeyPressed(KeyEvent.VK_S)&& lastKey != KeyEvent.VK_W) lastKey = KeyEvent.VK_S;
		else if (KeyBoard.iskeyPressed(KeyEvent.VK_W)&& lastKey != KeyEvent.VK_S) lastKey = KeyEvent.VK_W;
		else if (KeyBoard.iskeyPressed(KeyEvent.VK_A)&& lastKey != KeyEvent.VK_D) lastKey = KeyEvent.VK_A;
		else if (KeyBoard.iskeyPressed(KeyEvent.VK_D)&& lastKey != KeyEvent.VK_A) lastKey = KeyEvent.VK_D;
		if (KeyBoard.iskeyPressed(KeyEvent.VK_PLUS) && C.FPS > 10)
			C.FPS -= 5;
		else if (KeyBoard.iskeyPressed(KeyEvent.VK_MINUS))
			C.FPS += 5;
	}
	
	private Zelle getNextCell() {
		Zelle first = positionen.peek();
		
		/*
		 * Automate Snake Game: / close comment here to let algorithm play the
		 * game!
		 *
		 * 
		 * if (first.x + 1 == C.PANELWIDTH / C.TILESIZE && lastKey ==
		 * KeyEvent.VK_D) lastKey = KeyEvent.VK_S; else if (lastKey ==
		 * KeyEvent.VK_S && first.x == C.PANELWIDTH / C.TILESIZE - 1) lastKey =
		 * KeyEvent.VK_A; else if (lastKey == KeyEvent.VK_S && first.x == 1)
		 * lastKey = KeyEvent.VK_D; else if (first.x == 1 && first.y !=
		 * C.PANELHEIGHT / C.TILESIZE - 1 && lastKey == KeyEvent.VK_A) lastKey =
		 * KeyEvent.VK_S; else if (first.x == 0 && first.y == 0) lastKey =
		 * KeyEvent.VK_D; else if (first.x == 0) lastKey = KeyEvent.VK_W;
		 * 
		 * /* End of automation
		 */

		if(lastKey == KeyEvent.VK_S)return new Zelle(first.getX(), first.getY()+1);
		else if(lastKey == KeyEvent.VK_W) return new Zelle(first.getX(), first.getY()-1);
		else if(lastKey == KeyEvent.VK_A) return new Zelle(first.getX()-1, first.getY());
		else if(lastKey == KeyEvent.VK_D) return new Zelle(first.getX()+1, first.getY());
		else return new Zelle(first.getX(), first.getY()+1);
	}
	private void updateFood(){
		boolean created = false;
		int x, y;
		while(!created){
			x = r.nextInt((C.PANELWIDTH / C.TILESIZE));
			y = r.nextInt((C.PANELHEIGHT / C.TILESIZE));
			boolean overlapping = false;
			for (int i = 0;i<positionen.size();i++){
				Zelle temp = positionen.get(i);
				if((x == temp.getX() && y == temp.getY())) {overlapping = true;break;}
			}
			if (!overlapping){
				created = true;
				food = new Zelle(x, y);
			}
		}
	}
	
	
}
