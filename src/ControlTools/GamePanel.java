package controlTools;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.LinkedList;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class GamePanel extends JPanel{
	BufferedImage snaketiles, background;

	public GamePanel() {
		super();
		try {
			snaketiles = ImageIO.read(getClass().getResource("/res/snaketiles.png"));
			background = ImageIO.read(getClass().getResource("/res/background.png"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void paintComponent(Graphics g) {
		try {

			LinkedList<Zelle> positions = GameThread.positionen;
			Zelle food = GameThread.food;
			super.paintComponent(g);
			g.drawImage(background, 0, 0, null);
			// painting head
			switch (GameThread.lastKey) {
			case KeyEvent.VK_W:
				g.drawImage(snaketiles.getSubimage(192, 0, 64, 64), positions.get(0).x * 64, positions.get(0).y * 64,
						null);
				break;
			case KeyEvent.VK_S:
				g.drawImage(snaketiles.getSubimage(256, 64, 64, 64), positions.get(0).x * 64, positions.get(0).y * 64,
						null);
				break;
			case KeyEvent.VK_A:
				g.drawImage(snaketiles.getSubimage(192, 64, 64, 64), positions.get(0).x * 64, positions.get(0).y * 64,
						null);
				break;
			case KeyEvent.VK_D:
				g.drawImage(snaketiles.getSubimage(256, 0, 64, 64), positions.get(0).x * 64, positions.get(0).y * 64,
						null);
				break;

			}
			g.setColor(Color.WHITE);
			assert positions.size() > 2;
			Zelle before = positions.get(0);
			Zelle current = positions.get(1);
			Zelle next = positions.get(2);
			for (int cell = 3; cell < positions.size(); cell++) {
				drawCell(before, current, next, g, snaketiles);
				before = current;
				current = next;
				next = positions.get(cell);
			}
			drawCell(before, current, next, g, snaketiles);
			if (next.x == current.x) {
				if (next.y > current.y)
					g.drawImage(snaketiles.getSubimage(192, 128, 64, 64), next.x * 64, next.y * 64, null);
				else
					g.drawImage(snaketiles.getSubimage(256, 192, 64, 64), next.x * 64, next.y * 64, null);
			} else {
				if (next.x > current.x)
					g.drawImage(snaketiles.getSubimage(192, 192, 64, 64), next.x * 64, next.y * 64, null);
				else
					g.drawImage(snaketiles.getSubimage(256, 128, 64, 64), next.x * 64, next.y * 64, null);
			}
			g.drawImage(snaketiles.getSubimage(0, 192, 64, 64), food.x * 64, food.y * 64, null);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void drawCell(Zelle before, Zelle current, Zelle next, Graphics g, BufferedImage snaketiles) {
		if (before.x == current.x) {
			if (next.x == current.x)
				g.drawImage(snaketiles.getSubimage(128, 64, 64, 64), current.x * 64, current.y * 64, null);
			else if (next.x > current.x) {
				if (before.y < current.y)
					g.drawImage(snaketiles.getSubimage(0, 64, 64, 64), current.x * 64, current.y * 64, null);
				else
					g.drawImage(snaketiles.getSubimage(0, 0, 64, 64), current.x * 64, current.y * 64, null);
			} else {
				if (before.y < current.y)
					g.drawImage(snaketiles.getSubimage(128, 128, 64, 64), current.x * 64, current.y * 64, null);
				else
					g.drawImage(snaketiles.getSubimage(128, 0, 64, 64), current.x * 64, current.y * 64, null);
			}
		} else {
			if (next.y == current.y)
				g.drawImage(snaketiles.getSubimage(64, 0, 64, 64), current.x * 64, current.y * 64, null);
			else if (next.y > current.y) {
				if (before.x < current.x)
					g.drawImage(snaketiles.getSubimage(128, 0, 64, 64), current.x * 64, current.y * 64, null);
				else
					g.drawImage(snaketiles.getSubimage(0, 0, 64, 64), current.x * 64, current.y * 64, null);
			} else {
				if (before.x < current.x)
					g.drawImage(snaketiles.getSubimage(128, 128, 64, 64), current.x * 64, current.y * 64, null);
				else
					g.drawImage(snaketiles.getSubimage(0, 64, 64, 64), current.x * 64, current.y * 64, null);
			}
		}
	}
}
