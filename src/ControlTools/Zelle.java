package controlTools;

public class Zelle {
	
	int x, y;
	
	public Zelle(){
		
	}
	public Zelle(int x, int y){
		this.x = x;
		this.y = y;
	}
	public void setCoords(int x, int y){
		this.x = x;
		this.y = y;
	}
	public int getX(){
		return x;
	}
	public int getY(){
		return y;
	}
}
