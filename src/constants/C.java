package constants;

public abstract class C { // All Constants/Settings
	
	public static final int TILESIZE = 64; // size of a gametile in px
	public static int FPS = 110; // time between frames in ms - also influences
									// gamespeed
	public static final int PANELWIDTH = 1024; // width of gamepanel in px
	public static final int PANELHEIGHT = 896; // height of gamepanel in px
}