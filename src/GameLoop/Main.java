package gameLoop;

import controlTools.GameThread;

/*
 * README:
 * 
 * GAMERULES: Classic Snake, look it up on Google
 * 
 * CONTROLS: WASD to control snake, + and - to alter gamespeed
 * 
 * SETTINGS: You can adjust the settings in the constants.C.java file
 * 
 */

public class Main {

	public static void main(String[] args){
		GameThread t = new GameThread();
		t.start();
	}
	
	@SuppressWarnings("unused")
	private static void update(){
		
		// editable
	}
}
